//
//  MyCell.swift
//  Prova-trimestral-LucasSiqueira-3B2
//
//  Created by COTEMIG on 21/01/1444 AH.
//

import UIKit

class MyCell: UITableViewCell {
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var capa: UIImageView!
    @IBOutlet weak var musica: UILabel!
    @IBOutlet weak var autor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
